Invoke-WebRequest -Uri https://github.com/golangci/golangci-lint/releases/download/v1.23.6/golangci-lint-1.23.6-windows-amd64.zip -OutFile ".\download.zip"
Expand-Archive .\download.zip -DestinationPath .\download -Force
$p = Resolve-Path .\download\golangci-lint-1.23.6-windows-amd64
$Env:Path += ";$p"
[Environment]::SetEnvironmentVariable("Path", $env:Path, [System.EnvironmentVariableTarget]::Machine)
