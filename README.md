[![pipeline status](https://gitlab.com/polyapp-open-source/poly/badges/master/pipeline.svg)](https://gitlab.com/polyapp-open-source/poly/-/commits/master)
[![go report](https://goreportcard.com/badge/gitlab.com/polyapp-open-source/poly)](https://goreportcard.com/report/gitlab.com/polyapp-open-source/poly)
[![license](https://img.shields.io/badge/license-Apache%202-blue)](https://gitlab.com/polyapp-open-source/poly/-/blob/master/LICENSE)

# Purpose
Poly tests, builds, and runs multiple binaries. It first tries to perform work with the default GOOS and GOARCH and then tries again with GOOS=js GOARCH=wasm.
This is a simpler alternative to more flexible build tools like [Bazel](https://github.com/bazelbuild/rules_go).
Poly will replace bash scripts and makefiles in my programs because those are difficult to set up properly in CI/CD pipelines when testing cross-platform.
Plus poly is much simpler than those options and does not require configuration.

# Get Started
1. Navigate to your project directory. For instance, `git clone https://gitlab.com/polyapp-open-source/pwa` `cd pwa`
2. `go get gitlab.com/polyapp-open-source/poly`
3. `poly lint` requires https://github.com/golangci/golangci-lint to be installed.
Windows users can use ./installGolangciLint.ps1 which is used in .gitlab-ci.yml at "poly_demo_lint_windows". The Gitlab CI/CD script also installs 'diff'. 'diff' is included with git tools so you probably already have it on your dev machine.
4. `poly test`
5. `poly build`
6. `poly run` runs until all children have terminated, so if your program is a web server it won't terminate immediately.

That's it. `poly` doesn't offer many configuration options (`poly run --help`) and there are no verbosity or debugging settings.

If this works for you, use it. If it doesn't, don't use it. If it's close to working please leave an Issue.
