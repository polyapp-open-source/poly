Invoke-WebRequest -Uri https://github.com/tinygo-org/tinygo/releases/download/v0.12.0/tinygo0.12.0.windows-amd64.zip  -OutFile ".\tinygo.zip"
Expand-Archive .\tinygo.zip -DestinationPath .\tinygounzipped -Force
Move-Item -Path .\tinygounzipped\tinygo -Destination C:\
$Env:Path += ";C:\tinygo\bin"
$Env:TINYGOROOT = "C:\tinygo"
[Environment]::SetEnvironmentVariable("Path", $env:Path, [System.EnvironmentVariableTarget]::Machine)
