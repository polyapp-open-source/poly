choco install git -y -params '"/GitAndUnixToolsOnPath"'
# We must make sure the tools directory is added to PATH. By default this is the correct directory
# And although the above command is supposed to add it to Path, without this line golangci-lint fails
$Env:Path += ";C:\Program Files\Git\usr\bin"
[Environment]::SetEnvironmentVariable("Path", $env:Path, [System.EnvironmentVariableTarget]::Machine)
