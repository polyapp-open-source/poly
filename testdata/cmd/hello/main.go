package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Println("Hello from poly/testdata/cmd/hello/main.go")

	osCheck()
}

// osCheck returns true for success using syscalls and false otherwise
func osCheck() bool {
	_, err := os.Stat(".")
	if err != nil {
		fmt.Println("os.Stat failed: " + err.Error())
		return false
	}
	return true
}
