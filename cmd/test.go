package cmd

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"

	"github.com/spf13/cobra"
)

// testCmd represents the test command
//nolint
var testCmd = &cobra.Command{
	Use:   "test",
	Short: "Runs 'go test' across all main.go files",
	Long: `go test is run normally and also run with GOOS=js and GOARCH=wasm.
GOOS=js and GOARCH=wasm uses the default go_js_wasm_exec program. If
no such program is installed the user is notified and wasmbrowsertest is recommended.
GOOS=js and GOARCH=wasm makes an effort to not test code which could rely on non-js syscalls.
Results are collected and printed sequentially. Example output:

$ poly test
----- Command: C:\Go\bin\go.exe test gitlab.com/polyapp-open-source/poly/testdata/cmd ----- GOOS: windows GOARCH:  -----
?       gitlab.com/polyapp-open-source/poly/testdata/cmd        [no test files]
----- Command: C:\Go\bin\go.exe test gitlab.com/polyapp-open-source/poly/testdata/cmd/hello ----- GOOS: windows GOARCH:  -----
ok      gitlab.com/polyapp-open-source/poly/testdata/cmd/hello  (cached)
----- Command: C:\Go\bin\go.exe test gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm1 ----- GOOS: js GOARCH: wasm -----
?       gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm1  [no test files]
----- Command: C:\Go\bin\go.exe test gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm2 ----- GOOS: js GOARCH: wasm -----
?       gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm2  [no test files]
----- Command: C:\Go\bin\go.exe test gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm3 ----- GOOS: js GOARCH: wasm -----
?       gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm3  [no test files]
`,
	Run: func(cmd *cobra.Command, args []string) {
		exitCode := Test()
		os.Exit(exitCode)
	},
}

func init() {
	rootCmd.AddCommand(testCmd)
}

// Test performs the work of 'poly test'.
func Test() (exitCode int) {
	defaultTargets, JSWASMTargets := GetTargets()

	defaultProcs := make([]*procInfo, len(defaultTargets))
	for i, targetPath := range defaultTargets {
		defaultProc := procInfo{
			Cmd: exec.Command("go", "test", targetPath),
		}
		defaultProc.Cmd.Stdout = &defaultProc.Stdout
		defaultProc.Cmd.Stderr = &defaultProc.Stderr
		defaultProc.StartError = defaultProc.Cmd.Start()
		defaultProcs[i] = &defaultProc
	}

	JSWASMProcs := make([]*procInfo, len(JSWASMTargets))
	for i, targetPath := range JSWASMTargets {
		JSWASMProc := procInfo{
			Cmd: exec.Command("go", "test", targetPath),
		}
		JSWASMProc.Cmd.Env = append(os.Environ(), "GOOS=js", "GOARCH=wasm")
		JSWASMProc.Cmd.Stdout = &JSWASMProc.Stdout
		JSWASMProc.Cmd.Stderr = &JSWASMProc.Stderr
		JSWASMProc.StartError = JSWASMProc.Cmd.Start()
		JSWASMProcs[i] = &JSWASMProc
	}

	largestExitCode := 0
	for _, proc := range defaultProcs {
		proc.WaitError = proc.Cmd.Wait()
		proc.print()
		osExitValue := proc.getExitValue()
		if osExitValue > largestExitCode {
			largestExitCode = osExitValue
		}
	}
	for _, proc := range JSWASMProcs {
		if bytes.Contains(proc.Stdout.Bytes(), []byte("fork/exec")) {
			fmt.Println("----- Likely User Error Detected -----")
			fmt.Printf("Didn't see a go_js_wasm_exec. Run these commands:\n" +
				"go get github.com/agnivade/wasmbrowsertest\ncd $GOPATH/bin\n" +
				"mv wasmbrowsertest go_js_wasm_exec\n# add $GOBIN to $PATH if not already done\n" +
				"# Then this application should pass")
		}
		proc.WaitError = proc.Cmd.Wait()
		proc.print()
		osExitValue := proc.getExitValue()
		if osExitValue > largestExitCode {
			largestExitCode = osExitValue
		}
	}

	return largestExitCode
}
