package cmd

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/spf13/cobra"
)

// runCmd represents the run command
//nolint
var runCmd = &cobra.Command{
	Use:   "run",
	Short: "Runs go run across many main.go files",
	Long: `If GOOS and GOARCH for the exe are the default, it runs go run normally.
GOOS=js GOARCH=wasm it builds the program and deposits it in ./public or any other
directory specified by the WASMOut parameter with the .wasm file extension. Unlike other
commands, it prints the cmd first and shows output from cmds later. Stdout and
Stderr from each executable are streamed with a prefix so they get mixed in the output. 
Please note: this command is not safe to run in a production environment because
it starts a process with a function call as an argument. Example 'poly run' output:

$ poly run
----- Command: C:\Go\bin\go.exe build -ldflags=-s -w -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\public\wasm1.wasm gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm1 ----- GOOS: js GOARCH: wasm -----
----- Command: C:\Go\bin\go.exe build -ldflags=-s -w -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\public\wasm2.wasm gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm2 ----- GOOS: js GOARCH: wasm -----
----- Command: C:\Go\bin\go.exe build -ldflags=-s -w -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\public\wasm3.wasm gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm3 ----- GOOS: js GOARCH: wasm -----
----- Command: C:\tinygo\bin\tinygo.exe build -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\public\wasm1_tinygo.wasm -target wasm gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm1 ----- GOOS: windows GOARCH:  -----
----- Command: C:\tinygo\bin\tinygo.exe build -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\public\wasm2_tinygo.wasm -target wasm gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm2 ----- GOOS: windows GOARCH:  -----
----- Command: C:\tinygo\bin\tinygo.exe build -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\public\wasm3_tinygo.wasm -target wasm gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm3 ----- GOOS: windows GOARCH:  -----
----- Command: C:\Go\bin\go.exe build -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\cmd\cmd.exe gitlab.com/polyapp-open-source/poly/testdata/cmd ----- GOOS: windows GOARCH:  -----
----- Command: C:\Go\bin\go.exe build -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\cmd\hello\hello.exe gitlab.com/polyapp-open-source/poly/testdata/cmd/hello ----- GOOS: windows GOARCH:  -----
----- Command: C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\cmd\cmd.exe ----- GOOS: windows GOARCH:  -----
----- Command: C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\cmd\hello\hello.exe ----- GOOS: windows GOARCH:  -----
gitlab.com/polyapp-open-source/poly/testdata/cmd 18:28:30 Hello from poly/testdata/cmd/main.go
gitlab.com/polyapp-open-source/poly/testdata/cmd/hello 18:28:30 Hello from poly/testdata/cmd/hello/main.go
`,
	Run: func(cmd *cobra.Command, args []string) {
		exitCode := Run(cmd.Flag("WASMOut").Value.String())
		os.Exit(exitCode)
	},
}

func init() {
	rootCmd.AddCommand(runCmd)
	runCmd.Flags().String("WASMOut", "./public", "Place all compiled WASM binaries into this relative directory")
}

// Run performs the work of 'poly run'.
func Run(WASMOut string) (exitCode int) { //nolint
	defaultPaths, JSWASMPaths := GetTargets()

	largestExitCode := 0
	JSWASMProcs := startWASMBuild(JSWASMPaths, WASMOut)
	defaultBuildProcs := startDefaultBuild(defaultPaths)

	largestWASMExitCode := waitAndResolveWASMBuilds(JSWASMProcs, JSWASMPaths)
	if largestWASMExitCode > largestExitCode {
		largestExitCode = largestWASMExitCode
	}

	largestDefaultExitCode := waitAndResolveDefaultBuilds(defaultBuildProcs)
	if largestDefaultExitCode > largestExitCode {
		largestExitCode = largestDefaultExitCode
	}

	if largestExitCode > 0 {
		fmt.Println("poly run error: 'go build' failed at least once so the program will not 'run'")
		return largestExitCode
	}

	defaultExecProcs := createExecProcs(defaultPaths)

	for _, proc := range defaultExecProcs {
		proc.StartError = proc.Cmd.Start()
		if proc.StartError != nil {
			fmt.Println("Couldn't start cmd due to error. Cmd: " + proc.Cmd.String() + " Error:" + proc.StartError.Error())
		}
	}

	for _, proc := range defaultExecProcs {
		proc.WaitError = proc.Cmd.Wait()
		osExitValue := proc.getExitValue()
		if osExitValue > largestExitCode {
			largestExitCode = osExitValue
		}
	}

	return largestExitCode
}

// createExecProcs creates some procInfos which execute Go code built by this program or which happens to exist in the
// output directory targeted by this program. It does NOT start those procs.
func createExecProcs(defaultPaths []string) (execProcs []*procInfo) {
	execProcs = make([]*procInfo, len(defaultPaths))
	for i, targetPath := range defaultPaths {
		// unidiomatic explanation: the following lines are a security risk.
		defaultProc := procInfo{
			Cmd: exec.Command(filepath.Join(ListPathToAbsPath(targetPath), GetOutputFileName(targetPath, notWASM))), //nolint
		}
		lStd := log.New(os.Stdout, targetPath+" ", log.Ltime)
		lErr := log.New(os.Stderr, targetPath+" ", log.Ltime|log.Llongfile)
		rStdOut, err := defaultProc.Cmd.StdoutPipe()
		if err != nil {
			lStd.Println("couldn't init stdoutPipe: " + err.Error())
		}
		rStdErr, err := defaultProc.Cmd.StderrPipe()
		if err != nil {
			lErr.Println("couldn't init stdoutPipe: " + err.Error())
		}
		go func(l *log.Logger, r io.ReadCloser) {
			scanner := bufio.NewScanner(r)
			for scanner.Scan() {
				l.Println(scanner.Text())
			}
		}(lStd, rStdOut)
		go func(l *log.Logger, r io.ReadCloser) {
			scanner := bufio.NewScanner(r)
			for scanner.Scan() {
				l.Println(scanner.Text())
			}
		}(lErr, rStdErr)
		defaultProc.print() // might as well print out what we're building since stderr / stdout is streamed anyway
		execProcs[i] = &defaultProc
	}
	return execProcs
}
