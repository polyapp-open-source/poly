package cmd

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
)

const (
	notWASM = iota
	tinygoWASM
	regularGoWASM
)

type procInfo struct {
	Cmd        *exec.Cmd
	Stderr     bytes.Buffer
	Stdout     bytes.Buffer
	StartError error
	WaitError  error
}

// print out the procInfo and return the worst possible exit value
func (p *procInfo) print() {
	goos := os.Getenv("GOOS")
	goarch := os.Getenv("GOARCH")
	for _, envVar := range p.Cmd.Env {
		varName := strings.Split(envVar, "=")[0]
		if varName == "GOOS" {
			goos = strings.Split(envVar, "=")[1]
		} else if varName == "GOARCH" {
			goarch = strings.Split(envVar, "=")[1]
		}
	}
	fmt.Printf("----- Command: %v ----- GOOS: %v GOARCH: %v -----\n", p.Cmd.String(), goos, goarch)
	if p.StartError != nil {
		fmt.Println("Error starting command: " + p.StartError.Error())
	}
	// I'd like to print p.WaitError error, but os/exec considers a non-0 exit code an error out of Wait
	// fmt.Println("Error waiting for command: " + p.WaitError.Error())

	if p.Stderr.Len() > 0 {
		fmt.Println("----- Stderr -----")
		fmt.Print(p.Stderr.String())
	}
	if p.Stdout.Len() > 0 {
		if p.Stderr.Len() > 0 {
			// I don't love this, but if there is no Stderr I want to reduce the pollution of stdout with
			// ----- Stdout ----- every 3 lines which is common in  my test code right now.
			fmt.Println("----- Stdout -----")
		}
		fmt.Print(p.Stdout.String())
	}
}

func (p *procInfo) getExitValue() int {
	osExitValue := 0
	if p.WaitError != nil {
		if strings.HasPrefix(p.WaitError.Error(), "exit status ") {
			exitCodeString := strings.TrimPrefix(p.WaitError.Error(), "exit status ")
			var err error
			osExitValue, err = strconv.Atoi(exitCodeString)
			if err != nil {
				fmt.Println("Unexpected format in exit status: " + err.Error())
				osExitValue = 1
			}
		}
	}
	return osExitValue
}

// GetTargets runs 'go list' a couple of times to get some build / test / etc. targets
func GetTargets() (defaultPaths []string, JSWASMPaths []string) { //nolint
	// first get a list of relevant things
	defaultListCmd := procInfo{
		Cmd: exec.Command("go", "list", "./..."),
	}
	defaultListCmd.Cmd.Stdout = &defaultListCmd.Stdout
	defaultListCmd.Cmd.Stderr = &defaultListCmd.Stderr
	defaultListCmd.StartError = defaultListCmd.Cmd.Start()

	JSWASMListCmd := procInfo{
		Cmd: exec.Command("go", "list", "./..."),
	}
	JSWASMListCmd.Cmd.Env = append(os.Environ(), "GOOS=js", "GOARCH=wasm")
	JSWASMListCmd.Cmd.Stdout = &JSWASMListCmd.Stdout
	JSWASMListCmd.Cmd.Stderr = &JSWASMListCmd.Stderr
	JSWASMListCmd.StartError = JSWASMListCmd.Cmd.Start()

	defaultListCmd.WaitError = defaultListCmd.Cmd.Wait()
	JSWASMListCmd.WaitError = JSWASMListCmd.Cmd.Wait()

	for _, pI := range []procInfo{defaultListCmd, JSWASMListCmd} {
		if pI.Stderr.Len() > 0 && strings.Contains(pI.Stderr.String(), "matched no packages") {
			// this is OK - it means the user doesn't have any packages of this type to build
		} else if pI.StartError != nil || pI.WaitError != nil || pI.Stderr.Len() > 0 {
			fmt.Println("go list failure with default GOOS and GOARCH")
			if pI.StartError != nil {
				fmt.Println("start error: " + pI.StartError.Error())
			}
			if pI.WaitError != nil {
				fmt.Println("wait error: " + pI.WaitError.Error())
			}
			pI.print()
			os.Exit(1)
		}
	}

	defaultPaths = GetTargetsFromListOutput(defaultListCmd.Stdout)
	JSWASMPathsUnrevised := GetTargetsFromListOutput(JSWASMListCmd.Stdout)
	JSWASMPaths = make([]string, 0)
	for _, JSWASMPath := range JSWASMPathsUnrevised {
		includePath := true
		for _, defaultPath := range defaultPaths {
			if JSWASMPath == defaultPath {
				// won't compile things to both JS and default GOOS & GOARCH, so we skip this
				includePath = false
				break
			}
		}
		if includePath {
			JSWASMPaths = append(JSWASMPaths, JSWASMPath)
		}
	}

	return defaultPaths, JSWASMPaths
}

// ListPathToAbsPath converts a path seen from go list to an absolute path with forward slashes.
func ListPathToAbsPath(listPath string) (absolutePath string) {
	// actual relative path
	if strings.HasPrefix(listPath, ".") {
		var err error
		absolutePath, err = filepath.Abs(listPath)
		if err != nil {
			fmt.Println(`absolute path could not be generated from list path so path has been set to "": ` + err.Error())
			return ""
		}
		return absolutePath
	}

	if filepath.IsAbs(listPath) {
		return listPath
	}

	// path from $GOPATH/src
	absolutePath = os.Getenv("GOPATH")
	absolutePath = filepath.Join(absolutePath, "src", listPath)
	absolutePath = filepath.ToSlash(absolutePath) // sanitize the input GOPATH
	return absolutePath
}

// GetTargetsFromListOutput gives you relative paths to each 'go build' or 'go run' target.
func GetTargetsFromListOutput(list bytes.Buffer) (relativePaths []string) {
	relativePaths = make([]string, 0)
	pathsFromList := strings.Split(list.String(), "\n")
	for _, aPath := range pathsFromList {
		if strings.TrimSpace(aPath) != "" {
			relativePaths = append(relativePaths, aPath)
		}
	}
	return relativePaths
}

// GetOutputFileName gives you the output file name this program will generate given an input file.
func GetOutputFileName(filePath string, wasmType int) string {
	outputFileName := path.Base(filePath)
	if wasmType == tinygoWASM {
		return outputFileName + "_tinygo.wasm"
	} else if wasmType == regularGoWASM {
		return outputFileName + ".wasm"
	}
	if runtime.GOOS == "windows" { //nolint
		return outputFileName + ".exe"
	}
	return outputFileName
}
