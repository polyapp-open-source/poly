package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// autoCmd represents the auto command
var autoCmd = &cobra.Command{
	Use:   "auto",
	Short: "A highly opinionated automatic Lint, test, build, run, integration and deployment tool",
	Long:  `This tool is stupid and incredibly opinionated. It runs indefinitely.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("auto called")
	},
}

func init() {
	rootCmd.AddCommand(autoCmd)

	// Cobra supports local flags which will only run when this command is called
	// autoCmd.Flags().Int("issue", 0, "Given an Issue #, resolve the issue.")
}
