package cmd

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"

	"github.com/spf13/cobra"
)

// lintCmd represents the Lint command
//nolint
var lintCmd = &cobra.Command{
	Use:   "lint",
	Short: "Runs 'golangci-lint run --fix'",
	Long: `Uses configuration file in the current directory.
If none is found a default configuration is loaded.
Results are collected and printed one after the other. Example output:

$ poly.exe lint
cmd\hello\main_test.go:5:1: tests: Testmain has malformed name: first letter after 'Test' must not be lowercase (govet)
func Testmain(t *testing.T) {
^
golangci-lint failed: exit status 1

`,
	Run: func(cmd *cobra.Command, args []string) {
		exitCode := Lint()
		os.Exit(exitCode)
	},
}

func init() {
	rootCmd.AddCommand(lintCmd)
}

// Lint performs the work of 'poly lint'.
func Lint() (exitCode int) {
	configPath := ".golangci.yml"
	f, err := os.Open(configPath)
	if err != nil || f == nil {
		err = ioutil.WriteFile(configPath, []byte(configFile), 0644)
		if err != nil {
			fmt.Println("Couldn't create configuration file: " + err.Error())
			return 1
		}
		defer os.Remove(configPath)
	}
	f.Close()
	golangciLintCmd := exec.Command("golangci-lint", "run", "--fix", "--config", configPath)
	golangciLintCmd.Stderr = os.Stderr
	golangciLintCmd.Stdout = os.Stdout
	err = golangciLintCmd.Run()
	if err != nil {
		fmt.Println("golangci-lint failed: " + err.Error())
		if strings.Contains(err.Error(), `executable file not found`) {
			fmt.Println("golangci-lint not found. You should install it with these directions: https://github.com/golangci/golangci-lint#install ")
			return 1
		} else if err.Error() != "exit status 1" { //nolint
			fmt.Printf("\nnon-standard error code detected. Running golanglint-ci again in verbose mode\n\n")
			golangciLintCmdVerbose := exec.Command("golangci-lint", "run", "--fix", "--config", configPath, "-v")
			golangciLintCmdVerbose.Stderr = os.Stderr
			golangciLintCmdVerbose.Stdout = os.Stdout
			err = golangciLintCmdVerbose.Run()
			if err != nil {
				fmt.Println("golangci-lint failed: " + err.Error())
				return 1
			}
		}
	}
	return 0
}

var configFile = `
linters-settings:
  depguard:
    list-type: blacklist
    packages:
      # logging is allowed only by logutils.Log, logrus
      # is allowed to use only in logutils package
      - github.com/sirupsen/logrus
    packages-with-error-message:
      - github.com/sirupsen/logrus: "logging is allowed only by logutils.Log"
  dupl:
    threshold: 100
  funlen:
    lines: 100
    statements: 50
  goconst:
    min-len: 2
    min-occurrences: 3
  gocritic:
    enabled-tags:
      - diagnostic
      - experimental
      - opinionated
      - performance
      - style
    disabled-checks:
      - dupImport # https://github.com/go-critic/go-critic/issues/845
      - ifElseChain
      - octalLiteral
      - whyNoLint
      - wrapperFunc
  gocyclo:
    min-complexity: 15
  goimports:
    local-prefixes: github.com/golangci/golangci-lint
  golint:
    min-confidence: 0
  govet:
    check-shadowing: true
    settings:
      printf:
        funcs:
          - (github.com/golangci/golangci-lint/pkg/logutils.Log).Infof
          - (github.com/golangci/golangci-lint/pkg/logutils.Log).Warnf
          - (github.com/golangci/golangci-lint/pkg/logutils.Log).Errorf
          - (github.com/golangci/golangci-lint/pkg/logutils.Log).Fatalf
  lll:
    line-length: 140
  maligned:
    suggest-new: true
  misspell:
    locale: US

linters:
  disable-all: true
  enable:
    - bodyclose
    - deadcode
    - depguard
    - dogsled
    - dupl
    - errcheck
    - funlen
    - goconst
    - gocritic
    - gocyclo
    - gofmt
    - goimports
    - golint
    - gosec
    - gosimple
    - govet
    - ineffassign
    - interfacer
    - lll
    - misspell
    - nakedret
    - rowserrcheck
    - scopelint
    - staticcheck
    - structcheck
    - stylecheck
    - typecheck
    - unconvert
    - unparam
    - unused
    - varcheck
    - whitespace

  # don't enable:
  # - gochecknoglobals
  # - gocognit
  # - godox
  # - maligned
  # - prealloc

issues:
  # Excluding configuration per-path, per-linter, per-text and per-source
  exclude-rules:
    - path: _test\.go
      linters:
        - gomnd

run:
  skip-dirs:
    - test/testdata_etc
    - internal/cache
    - internal/renameio
    - internal/robustio

# golangci.com configuration
# https://github.com/golangci/golangci/wiki/Configuration
service:
  golangci-lint-version: 1.23.x # use the fixed version to not introduce new linters unexpectedly
  prepare:
    - echo "here I can run custom commands, but no preparation needed for this repo"
`
