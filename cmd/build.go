package cmd

import (
	"os"
	"os/exec"
	"path/filepath"

	"github.com/spf13/cobra"
)

// buildCmd represents the build command
//nolint
var buildCmd = &cobra.Command{
	Use:   "build",
	Short: "Runs go build across all main.go files",
	Long: `Runs 'go list' to get a list of packages and then runs 'go build'
across each package individually with standard & GOOS=js GOARCH=wasm environments.
If GOOS=js GOARCH=wasm it builds the file and deposits it in ./public or
any other directory specified by the WASMOut parameter with the .wasm file extension.
If tinygo is installed it is used when GOOS=js GOARCH=wasm and the result is saved
as buildname_tinygo.wasm alongside the 'go build' output, buildname.wasm
GOOS=js and GOARCH=wasm makes an effort to not build code which could rely on non-js syscalls.
Results are collected and printed one after the other. Example output:

gledr@LAPTOP-AH6IAHBO MINGW64 ~/Polyapp_Apps/gocode/src/gitlab.com/polyapp-open-source/poly/testdata (CI-expansion)
$ poly build
----- Command: C:\Go\bin\go.exe build -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\cmd\cmd.exe gitlab.com/polyapp-open-source/poly/testdata/cmd ----- GOOS: windows GOARCH:  -----
----- Command: C:\Go\bin\go.exe build -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\cmd\hello\hello.exe gitlab.com/polyapp-open-source/poly/testdata/cmd/hello ----- GOOS: windows GOARCH:  -----
----- Command: C:\Go\bin\go.exe build -ldflags=-s -w -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\cmd\wasm1\wasm1.wasm gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm1 ----- GOOS: js GOARCH: wasm -----
----- Command: C:\Go\bin\go.exe build -ldflags=-s -w -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\cmd\wasm2\wasm2.wasm gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm2 ----- GOOS: js GOARCH: wasm -----
----- Command: C:\Go\bin\go.exe build -ldflags=-s -w -o C:\Users\gledr\Polyapp_Apps\gocode\src\gitlab.com\polyapp-open-source\poly\testdata\cmd\wasm3\wasm3.wasm gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm3 ----- GOOS: js GOARCH: wasm -----

`,
	Run: func(cmd *cobra.Command, args []string) {
		exitCode := Build(cmd.Flag("WASMOut").Value.String())
		os.Exit(exitCode)
	},
}

func init() {
	rootCmd.AddCommand(buildCmd)
	buildCmd.Flags().String("WASMOut", "./public", "Place all compiled WASM binaries into this relative directory")
}

// Build performs the work of 'poly build'.
func Build(WASMOut string) (exitCode int) { //nolint
	defaultPaths, JSWASMPaths := GetTargets()

	defaultProcs := startDefaultBuild(defaultPaths)

	JSWASMProcs := startWASMBuild(JSWASMPaths, WASMOut)

	largestExitCode := 0
	largestDefaultExitCode := waitAndResolveDefaultBuilds(defaultProcs)
	if largestDefaultExitCode > largestExitCode {
		largestExitCode = largestDefaultExitCode
	}
	largestWASMExitCode := waitAndResolveWASMBuilds(JSWASMProcs, JSWASMPaths)
	if largestWASMExitCode > largestExitCode {
		largestExitCode = largestWASMExitCode
	}

	return largestExitCode
}

// startWASMBuild spawns some processes to being a WASM build.
// We try to compile with both tinygo and regular 'go build' every time.
func startWASMBuild(JSWASMPaths []string, wasmOutPath string) (JSWASMProcs []*procInfo) { //nolint
	JSWASMProcs = make([]*procInfo, len(JSWASMPaths)*2)
	for i, targetPath := range JSWASMPaths {
		var outputPath string
		if wasmOutPath == "" {
			outputPath = filepath.Join(ListPathToAbsPath(targetPath), GetOutputFileName(targetPath, regularGoWASM))
		} else {
			outputPath = filepath.Join(ListPathToAbsPath(wasmOutPath), GetOutputFileName(targetPath, regularGoWASM))
		}
		JSWASMProc := procInfo{
			Cmd: exec.Command("go", "build", "-ldflags=-s -w", "-o", outputPath, targetPath),
		}
		JSWASMProc.Cmd.Env = append(os.Environ(), "GOOS=js", "GOARCH=wasm")
		JSWASMProc.Cmd.Stdout = &JSWASMProc.Stdout
		JSWASMProc.Cmd.Stderr = &JSWASMProc.Stderr
		JSWASMProc.StartError = JSWASMProc.Cmd.Start()
		JSWASMProcs[i] = &JSWASMProc
	}
	for i, targetPath := range JSWASMPaths {
		var outputPath string
		if wasmOutPath == "" {
			outputPath = filepath.Join(ListPathToAbsPath(targetPath), GetOutputFileName(targetPath, tinygoWASM))
		} else {
			outputPath = filepath.Join(ListPathToAbsPath(wasmOutPath), GetOutputFileName(targetPath, tinygoWASM))
		}
		JSWASMProc := procInfo{
			Cmd: exec.Command("tinygo", "build", "-o", outputPath, "-target", "wasm", targetPath),
		}
		JSWASMProc.Cmd.Stdout = &JSWASMProc.Stdout
		JSWASMProc.Cmd.Stderr = &JSWASMProc.Stderr
		JSWASMProc.StartError = JSWASMProc.Cmd.Start()
		JSWASMProcs[i+len(JSWASMPaths)] = &JSWASMProc
	}
	return JSWASMProcs
}

func startDefaultBuild(defaultPaths []string) (defaultProcs []*procInfo) {
	defaultProcs = make([]*procInfo, len(defaultPaths))
	for i, targetPath := range defaultPaths {
		outputPath := filepath.Join(ListPathToAbsPath(targetPath), GetOutputFileName(targetPath, notWASM))
		defaultProc := procInfo{
			Cmd: exec.Command("go", "build", "-o", outputPath, targetPath),
		}
		defaultProc.Cmd.Stdout = &defaultProc.Stdout
		defaultProc.Cmd.Stderr = &defaultProc.Stderr
		defaultProc.StartError = defaultProc.Cmd.Start()
		defaultProcs[i] = &defaultProc
	}
	return defaultProcs
}

// waitAndResolveWASMBuilds waits for the tinygo and regular 'go build' WASM builds.
// this function ignores tinygo errors when computing largestExitCode.
func waitAndResolveWASMBuilds(JSWASMProcs []*procInfo, JSWASMPaths []string) (largestExitCode int) { //nolint
	largestExitCode = 0
	for i, proc := range JSWASMProcs {
		proc.WaitError = proc.Cmd.Wait()
		proc.print()
		osExitValue := proc.getExitValue()
		if i < len(JSWASMPaths) {
			// go build
			if osExitValue > largestExitCode {
				largestExitCode = osExitValue
			}
		}
		// else tinygo build - don't report tinygo's exit code since tinygo need not be installed for build to succeed
	}
	return largestExitCode
}

// waitAndResolveDefaultBuilds waits for the default builds to finish and reports the largest exit code.
func waitAndResolveDefaultBuilds(defaultProcs []*procInfo) (largestExitCode int) { //nolint
	for _, proc := range defaultProcs {
		proc.WaitError = proc.Cmd.Wait()
		proc.print()
		osExitValue := proc.getExitValue()
		if osExitValue > largestExitCode {
			largestExitCode = osExitValue
		}
	}
	return largestExitCode
}
