package cmd

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
)

func ExampleTest() {
	// get somewhere we can safely test
	wd, err := os.Getwd()
	if err != nil {
		fmt.Printf("Couldn't get working directory: %v\n", err.Error())
	}
	if path.Base(filepath.ToSlash(wd)) != "testdata" {
		// not good. Don't want to run Test() in poly/cmd/test since that will call TestTest() which calls Test()
		// which calls TestTest() which calls Test() which calls TestTest() which calls Test()...
		err = os.Chdir("./testdata")
		if err != nil {
			// could probably hunt around more but why bother
			wd, _ := os.Getwd()
			fmt.Println("Couldn't figure out how to get to testdata directory. os.Getwd() output: " + wd)
		}
	}

	exitCode := Test()
	if exitCode != 0 {
		fmt.Printf("ExampleTest failed with exit code: %v\n", exitCode)
	}
}
