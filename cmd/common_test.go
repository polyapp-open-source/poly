package cmd

import (
	"bytes"
	"fmt"
	"os"
	"runtime"
	"strings"
	"testing"
)

// ExampleGetTargets is used in lieu of measuring stdout
func ExampleGetTargets() {
	err := os.Chdir("../testdata")
	if err != nil {
		fmt.Println("Error changing to testdata directory: " + err.Error())
	}
	defaultPaths, JSWASMPaths := GetTargets()
	fmt.Println(defaultPaths)
	fmt.Println(JSWASMPaths)
	//nolint
	// Output:
	// [gitlab.com/polyapp-open-source/poly/testdata/cmd gitlab.com/polyapp-open-source/poly/testdata/cmd/hello]
	// [gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm1 gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm2 gitlab.com/polyapp-open-source/poly/testdata/cmd/wasm3]
}

func TestGetOutputFileName(t *testing.T) {
	fn := GetOutputFileName("/tmp/something", notWASM)
	if runtime.GOOS == "windows" && fn != "something.exe" {
		t.Error("expected file to be named 'something.exe' and got: " + fn)
	} else if runtime.GOOS != "windows" && fn != "something" {
		t.Error("expected file to be named 'something' and got: " + fn)
	}

	expectedFileName := "somethingelse_tinygo.wasm"
	fn = GetOutputFileName("./tmp/somethingelse", tinygoWASM)
	if runtime.GOOS == "windows" && fn != expectedFileName {
		t.Error("expected file to be named '" + expectedFileName + "' and got: " + fn)
	} else if runtime.GOOS != "windows" && fn != expectedFileName {
		t.Error("expected file to be named '" + expectedFileName + "' and got: " + fn)
	}

	expectedFileName = "somethingelse.wasm"
	fn = GetOutputFileName("./somethingelse", regularGoWASM)
	if runtime.GOOS == "windows" && fn != expectedFileName {
		t.Error("expected file to be named '" + expectedFileName + "' and got: " + fn)
	} else if runtime.GOOS != "windows" && fn != expectedFileName {
		t.Error("expected file to be named '" + expectedFileName + "' and got: " + fn)
	}
}

func TestGetTargetsFromListOutput(t *testing.T) {
	var list bytes.Buffer
	list.WriteString(`gitlab.com/polyapp-open-source/poly/testdata/cmd
gitlab.com/polyapp-open-source/poly/testdata/cmd/hello

`)
	targets := GetTargetsFromListOutput(list)
	if len(targets) != 2 {
		t.Error("Expected 2 targets based on the output of go list")
	}
	if targets[0] != "gitlab.com/polyapp-open-source/poly/testdata/cmd" {
		t.Error("Didn't contain the expected first path")
	}
	if targets[1] != "gitlab.com/polyapp-open-source/poly/testdata/cmd/hello" {
		t.Error("Didn't contain the expected second path")
	}
}

func TestListPathToAbsPath(t *testing.T) {
	in := "gitlab.com/polyapp-open-source/poly/testdata/cmd"
	desiredOutSubstring := `src/gitlab.com/polyapp-open-source/poly/testdata/cmd`
	actualOut := ListPathToAbsPath(in)
	if !strings.Contains(actualOut, desiredOutSubstring) {
		t.Errorf("in: %s \nout: %s \ndesired substring: %s\n", in, actualOut, desiredOutSubstring)
	}
}
