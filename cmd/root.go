package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "poly",
	Short: "Test, Build, Run, and Serve many Go applications at once without any configuration",
	Long: `Poly is for WASM apps which require cross-platform builds. It can lint, test, build,
and run >1 executable, and then it does so a second time with GOOS=js and GOARCH=wasm.
Poly cannot be configured, although tools called by Poly (golangci-lint) can be.

Simple apps can use 'go build' to immediately build all of their code, which is ideal.
Moderately complex apps use scripts or makefiles, but those aren't cross-platform and
require learning another language. Very complex apps can use Bazel or a similar build 
tool, which has even more overhead than scripts and makefiles.

This program - especially 'poly run' - is not designed for production environments.
`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
